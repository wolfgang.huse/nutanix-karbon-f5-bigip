Integration of Nutanix Karbon Kubernetes Cluster into f5 BigIP Load-Balancer

This Guide is based on a BigIP 15.0 Single NIC Applicance
BigIP Interface IP: 10.42.49.77
POD CIDR of K8s: 172.20.0.0/16

# create vxlan profile
create net tunnels vxlan k8s-vxlan { app-service none port 8472 flooding-type none }

# create VTEP (use SelfIP of BigIP-Device as local Address)
create net tunnels tunnel k8s-tunnel { app-service none key 1 local-address 10.42.49.77 profile k8s-vxlan }

# create SelfIP for POD CIDR
create net self k8s-vxlan-selfip { address 172.20.255.100/16 vlan k8s-tunnel allow-service all }

# determine VTEP MAC 
show net tunnels tunnel k8s-tunnel all-properties

# create partition
create auth partition kubernetes 

## Dummy-Node.yml
kubectl apply -f dummynode.yml

# Store BigIP Login as Secret
kubectl create secret generic bigip-ctlr-secret --namespace kube-system --from-literal=username=admin --from-literal=password=nutanix/4u

# create Service Account
kubectl create serviceaccount bigip-ctlr -n kube-system

# create ClusterRole and Role-Binding
kubectl apply -f rbac.yml

# deploy BigIP-Controller
kubectl apply -f ctlr.yml


### Ready for Demo-Time

# deploy Hello-World-Pods
kubectl apply -f deployment-hello-world.yml

#deploy Service (ClusterIP)
kubectl apply -f clusterip-service-hello-world.yml

#deploy Ingress Config
kubectl apply -f ingress-hello-world.yml
